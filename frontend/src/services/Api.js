import axios from 'axios';

const api = axios.create({ baseURL: "https://5pnnhw5h8d.execute-api.us-east-1.amazonaws.com/v1"});

export default api;