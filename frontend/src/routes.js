import StartUpPage from './pages/startup/StartUpPage'

const Routes = () => [
    {
        path:"/startuppage",
        component: StartUpPage
    }
]

export default Routes;