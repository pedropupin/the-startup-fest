import React, { Component } from 'react';
import api from '../../services/Api';

export default class Main extends Component{
   state = {
       startup: []
   }
   
   
    componentDidMount(){
        this.loadStartup();
    }

    loadStartup = async () => {
        const response = await api.get("/startup");

        this.setState({ startup: response.data.docs})

        console.log(response.data.docs);
    };
   
    render(){
    return (
        <div className="startup-list">
            {this.state.startup.map(startup => (
                <h2> key={startup.id}</h2>
            ))}

        </div>
    )
    }
}
