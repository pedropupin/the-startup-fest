import React from 'react';
import Header from './components/header/Header';
import Main from './pages/main/Index'

import './App.css';

const  App = () => {
  return (
    <div className="App">
     <Header />
     <Main />
    </div>
  );
}

export default App;
