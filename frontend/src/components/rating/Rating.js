import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import './Rating.css'; 

const StartRating = () => {
  const [value, setValue] = React.useState(2);

  return (
    <div className="description">
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Typography component="legend">Proposta</Typography>
        <p>Qual foi a sua satisfação ao ouvir esta proposta?</p>
        <Rating
          name="simple-controlled"
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        />
      </Box>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Typography component="legend">Apresentação / Pitch</Typography>
        <p>A StartUp soube apresentar o seu produto?</p>
        <Rating
          name="simple-controlled"
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        />
      </Box>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Typography component="legend">Desenvolvimento</Typography>
        <p>O produto / serviço atende bem o que foi proposto?</p>
        <Rating
          name="simple-controlled"
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        />
      </Box>
    </div>
  );
}

export default StartRating;