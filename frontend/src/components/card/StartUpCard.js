import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import StartRating from '../rating/Rating';
import './StartUpCard.css'

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

const StartUpCard = () => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image="/static/images/cards/contemplative-reptile.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Nome da StartUp
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Dummy text Dummy text Dummy text Dummy text Dummy textDummy text 
            Dummy text 
          </Typography>
        </CardContent>
        <StartRating />
      </CardActionArea>
    </Card>
  );
}

export default StartUpCard;